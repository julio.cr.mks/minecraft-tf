#! /bin/bash
sudo yum update -y
sudo yum install java-17-amazon-corretto-headless -y
sudo adduser minecraft
sudo su
mkdir /opt/minecraft
mkdir /opt/minecraft/server
cd /opt/minecraft/server
sudo wget https://piston-data.mojang.com/v1/objects/f69c284232d7c7580bd89a5a4931c3581eae1378/server.jar
sudo chown -R minecraft:minecraft /opt/minecraft/
echo "[Unit]
Description=Minecraft Server
After=network.target

[Service]
Nice=5

User=root
WorkingDirectory=/opt/minecraft/server
ReadWriteDirectories=/opt/minecraft/server
ExecStart=/usr/bin/java -Xmx8024M -Xms1024M -jar server.jar nogui


[Install]
WantedBy=multi-user.target
" > /etc/systemd/system/minecraft.service
sudo /usr/bin/java -Xmx8024M -Xms1024M -jar server.jar nogui
echo "eula=true" > eula.txt
chmod 664 /etc/systemd/system/minecraft.service
systemctl daemon-reload
sudo systemctl start minecraft.service