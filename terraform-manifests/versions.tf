#Terraform block
terraform{
    required_version = "~> 1.2.2"
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "~> 3.0"

        }
        random = {
            source  = "hashicorp/random"
            version = ">= 2.0"
        }
        null = {
            source  = "hashicorp/null"
            version = ">= 2.0"
        }
    }
}

# Provider Block
provider "aws" {
    region = var.aws_region
    # AWS_ACCESS_KEY_ID
    # AWS_SECRET_ACCESS_KEY
}
